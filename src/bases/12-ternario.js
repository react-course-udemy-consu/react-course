//Operador condicional ternario
//Vamos a entregar un mensaje dependiendo si esta activo
//o no el usuario
const activo = true;
/* let mensaje = '';

if (activo) {
  mensaje = 'Activo';
} else {
  mensaje = 'Inactivo';
} */

//Pero ahora queremos hacer una asignación a una variable que en 
//este caso sería mensaje
const mensaje = (!activo) ? 'Activo' : 'Inactivo';

//Hay ocaciones donde solo se quiere ejecutar algo cuando se
//cumple la condición y en caso de que no no se quiere hacer nada
//para esto se escribe así:
/* const mensaje = (activo) ? 'Activo' : null; */

//Otra manera de hacer esto sería así:
/* const mensaje = activo && 'Activo'; */
/* const mensaje = (activo === true) && 'Activo';
 */
console.log(mensaje);
