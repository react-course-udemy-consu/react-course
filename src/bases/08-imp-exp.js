import heroes, { owners } from '../data/heroes';

/* console.log(heroes);
console.log(owners); */

//Ejercicio
export const getHeroeById = (id) => heroes.find( (heroe) => heroe.id === id);
/* console.log( getHeroeById(3) ); */

//También se puede hacer la siguiente función, pero es más largo
/* const getHeroeById = (id) => {
    return heroes.find( (heroe) => {
        if (heroe.id === id) {
            return true;
        } else {
            return false;
        }
    });
} */
//Ejercicio, para este ejercicio hay que utilizar filter() porque find()solo trae uno objeto
//De la siguiente función hacer que funcione console.log(getHeroesByOwner())
export const getHeroesByOwner = ( owner ) => heroes.filter( (heroe) => heroe.owner === owner);

/* console.log( getHeroesByOwner('Marvel'));
console.log( getHeroesByOwner('DC')); */
