//Async-Await
//vamos a replicar el ejercicio anterior pero con async y await
/* const apiKey = 'o7AD2doak789T58L5BNEyzedZdILAwSD';
const peticion = fetch(`https://api.giphy.com/v1/gifs/random?api_key=${ apiKey }`);

peticion
  .then( resp => resp.json())
  .then( ({data}) => {
    const {url} = data.images.original;
    console.log(url);

    const img = document.createElement('img');
    img.src = url;

    document.body.append(img);
  })
  .catch(console.warn); */
//Para manejar el error se utiliza el Try y el Catch
const getImage = async() => {
  try {
    const apiKey = 'o7AD2doak789T58L5BNEyzedZdILAwSD';
    const resp = await fetch(`https://api.giphy.com/v1/gifs/random?api_key=${ apiKey }`);
    const {data} = await resp.json();
    console.log(data);
    //Tarea, tomar la data y mostrarla en el html
    const {url} = data.images.original;
    const img = document.createElement('img');
    img.src = url;

    document.body.append(img);
  } catch (error) {
    //manejo del error
    console.error(error);
  } 
}
//La unica condición para trabajar con await es que tiene que estar
//dentro de una función async.
getImage();