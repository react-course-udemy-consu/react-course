//Desestructuración de Arreglos
const personajes = ['Goku', 'Vegeta', 'Trunks'];

console.log(personajes);
console.log(personajes[0]);
console.log(personajes[1]);
console.log(personajes[2]);

//Una forma de desestructurar arreglos es la siguiente:
const[p1] = personajes;
console.log(p1);
const[ , p2] = personajes;
console.log(p2);

//probemos con una función:
const retornoArreglo = () => {
    return ['ABC', 123];
}

const [ letras, numeros ] = retornoArreglo();
console.log(letras, numeros);

//Tarea
//1. el primer valor del arr se llamará nombre
//2. el segundo valor se llamará setNombre
const arregloConFuncion = (valor) => {
    return [valor, () => {console.log('Hola Mundo')}];
}

const arr = arregloConFuncion('Goku');
/* console.log(arr); */
/* arr[1](); */

const [nombre, setNombre] = arregloConFuncion('Consu');
console.log(nombre);
setNombre();