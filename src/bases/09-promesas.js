//Promesas

import { getHeroeById } from "./bases/08-imp-exp";

/* const promesa = new Promise( (resolve, reject) => {
  setTimeout(() => {
    //Tarea
    //importar el 
    const p1 = getHeroeById(4);
    //console.log(heroe);
    resolve(p1);
  }, 2000);
}); 

promesa.then( (heroe) => {
  console.log('heroe', heroe)
})
.catch(err => console.warn(err)); */

const getHeroeByIdAsync = (id) => {
  return new Promise( (resolve, reject) => {
    setTimeout(() => {
      const p1 = getHeroeById(id);
      if (p1) {
        resolve(p1);
      } else {
        reject('No se ha logrado encontrar el héroe');
      }
      //Tarea agregar el reject y catch
    }, 2000);
  }); 
}

getHeroeByIdAsync(1)
.then(console.log)
.catch(err => console.warn(err));
  