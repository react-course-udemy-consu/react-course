//Destructuración de Objetos o Asingación Desestructurante

const persona = {
    nombre: 'Tony',
    edad: 45,
    clave: 'ironman'
};
/* console.log(persona.nombre);
console.log(persona.edad);
console.log(persona.clave); */

/* en el ejemplo anterior para obtener los datos que son de interes
se debe escribir persona.nombre (ejemplo), pero se puede hacer de 
forma distinta para extraer la información de interes con la asignación
desestructurante: */

/* const persona2 = {
    nombre: 'Clara',
    edad: 35,
    clave: 'aquaman'
};

const { nombre, edad, clave } = persona2;
console.log( nombre );
console.log( edad ); */

const holaHola = ({ clave, nombre, edad }) => {
    /* console.log(nombre, edad, rango); */
    return {
        nombreClave: clave,
        anios: edad,
        latlng: {
            lat: 12.3045,
            lng: -14.0874
        }
    }
}

const {nombreClave, anios, latlng: { lat, lng }} = holaHola( persona );

console.log( nombreClave, anios );
console.log( lat, lng );

