/* Objetos literales:
los objetos literales son también conocidos como diccionarios y trabajando con pares de valores
const persona = {
    nombre: 'Tony'
}
"nombre" es la llave y el valor al que apunta esa llave es " 'Tony' "
*/
const persona = {
    nombre: 'Tony',
    apellido: 'Stark',
    edad: 45,
    direccion: {
        ciudad: 'New York',
        zip: 575493,
        lat: 14.88383,
        lng: 34.56788
    }
};

console.log(persona);
console.log( {persona} );
console.table(persona);

const persona2 = { ...persona };
persona2.nombre = 'Clara';

console.log(persona);
console.log(persona2);