/* Variables y Constantes  */
const nombre = 'Consuelo';
const apellido = 'Anguita';

let valorDado = 5;
valorDado = 4;

console.log(nombre, apellido, valorDado);

if (true) {
    let valorDado = 6;
    console.log(valorDado);
}

console.log(valorDado);

/* el valorDado dentro del if (scoope) solo va a tener ese valor dentro, fuera del scoope
tiene el valor que se le fue asignado en la línea 4 y luego cambia a la nueva asignación
en la línea 5 */