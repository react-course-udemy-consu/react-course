//Fetch Api
const apiKey = 'o7AD2doak789T58L5BNEyzedZdILAwSD';
const peticion = fetch(`https://api.giphy.com/v1/gifs/random?api_key=${ apiKey }`);

//Esto se conoce como promesas en cadena, el resultado de una promesa pasa al
//siguiente then, y así sucesivamente
/* peticion
  .then( resp => resp.json() )
  .then( data => {
    console.log(data)
  })
.catch(console.warn); */

//La data se puede extraer mediante desestructuración
peticion
  .then( resp => resp.json())
  .then( ({data}) => {
    const {url} = data.images.original;
    console.log(url);

    const img = document.createElement('img');
    img.src = url;

    document.body.append(img);
  })
  .catch(console.warn);