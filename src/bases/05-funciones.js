/* Funciones en JS */

const saludar = function(nombre) {
    return `Hola, ${nombre}`;
}

const saludar2 = (nombre) => {
    return `Hola, ${nombre}`;
}

const saludar3 = (nombre) => `Hola, ${nombre}`;
/* Esta funcion es la misma que saludar2 solo que de forma mas simplificada */

console.log(saludar('Goku'));
console.log(saludar2('Carmen'));
console.log(saludar3('Clara'));

/* Funcion que retorna un objeto */
const getUser = () => {
    return {
        uid: 'ABDGG123',
        username: 'Catita21'
    }
}
/* La misma función, pero sin el return */
const getUser2 = () => ({
    uid: 'ABDGG123',
    username: 'Catita21' 
})
console.log(getUser());
console.log(getUser2());

//Tarea
//1. Transformar a una  función de flecha
//2. Tiene que retornar un objeto implicito
//3. Hay que hacer pruebas

/* function getActiveUser( nombre) {
    return {
        uid: 'FFGTE123',
        username: nombre
    }
}; */

const getActiveUser = (nombre) => ({
    uid: 'FFGTE123',
    username: nombre
});

const activeUser = getActiveUser('Conie')
console.log(activeUser);